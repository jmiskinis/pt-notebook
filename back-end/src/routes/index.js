import { signUpRoute } from "./user/signUpRoute.js";
import { signInRoute } from "./user/signInRoute.js";
import { verifyEmailRoute } from "./user/verifyEmailRoute.js";
import { forgotPasswordRoute } from "./user/forgotPasswordRoute.js";
import { resetPasswordRoute } from "./user/resetPasswordRoute.js";
import { getUserEmailVerificationStatusRoute } from "./user/getUserEmailVerificationStatusRoute.js";
import { resendConfirmationCodeRoute } from "./user/resendConfirmationCodeRoute.js";
import { getScratchpadRoute } from "./notebook/getScratchpadRoute.js";
import { getFavoritesRoute } from "./notebook/getFavoritesRoute.js";
import { getTrashRoute } from "./notebook/getTrashRoute.js";
import { getNotesRoute } from "./notebook/getNotesRoute.js";
import { markAsFavoriteRoute } from "./notebook/markAsFavoriteRoute.js";
import { removeFromFavoritesRoute } from "./notebook/removeFromFavoritesRoute.js";
import { markAsTrashedRoute } from "./notebook/markAsTrashedRoute.js";
import { restoreFromTrashedRoute } from "./notebook/restoreFromTrashedRoute.js";
import { removePermanentlyRoute } from "./notebook/removePermanentlyRoute.js";
import { createNewNoteRoute } from "./notebook/createNewNoteRoute.js";
import { updateNoteRoute } from "./notebook/updateNoteRoute.js";
import { updateScratchpadRoute } from "./notebook/updateScratchpadRoute.js";

export const routes = [
    getScratchpadRoute,
    getFavoritesRoute,
    getTrashRoute,
    updateScratchpadRoute,
    signUpRoute,
    signInRoute,
    verifyEmailRoute,
    forgotPasswordRoute,
    resetPasswordRoute,
    resendConfirmationCodeRoute,
    getUserEmailVerificationStatusRoute,
    removePermanentlyRoute,
    getNotesRoute,
    markAsFavoriteRoute,
    removeFromFavoritesRoute,
    markAsTrashedRoute,
    restoreFromTrashedRoute,
    createNewNoteRoute,
    updateNoteRoute,
];