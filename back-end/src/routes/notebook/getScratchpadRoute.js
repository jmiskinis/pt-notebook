import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const getScratchpadRoute = {
    path: "/api/v1/notes/scratchpad/:aws_sub",
    method: "get",
    handler: async (request, response) => {
        const { aws_sub } = request.params;

        if (!aws_sub) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const notes = await db.collection("notes").findOne(
            {
                "aws_sub": aws_sub
            }
        );

        if (notes) {
            return response.json(notes.scratchpad);
        } else {
            return response.sendStatus(404);
        }
    },
};