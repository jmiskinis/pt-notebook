import { ObjectId } from "mongodb";
import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const updateNoteRoute = {
    path: "/api/v1/notes/:uid",
    method: "patch",
    handler: async (request, response) => {
        const { aws_sub } = request.body;
        const { uid } = request.params;

        if (!aws_sub || !uid) {
            return response.status(401).json("Missing parameters");
        }

        const { heading, placeholder, content } = request.body;
        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const status = await db.collection("notes").updateOne(
            {
                "aws_sub": aws_sub,
            },
            {
                "$set": {
                    "notes.$[x].heading": heading,
                    "notes.$[x].placeholder": placeholder,
                    "notes.$[x].content": content,
                    "notes.$[x].dateLastSaved": new Date().getTime(),
                }
            },
            {
                "arrayFilters": [
                    {
                        "x._id": new ObjectId(uid)
                    }
                ]
            }
        );

        return response.json(status);
    },
};