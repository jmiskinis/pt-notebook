import { ObjectId } from "mongodb";
import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const restoreFromTrashedRoute = {
    path: "/api/v1/notes/:uid/restore-from-trashed",
    method: "patch",
    handler: async (request, response) => {
        const { uid } = request.params;
        const { aws_sub } = request.body;

        if (!aws_sub || !uid) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const status = await db.collection("notes").updateOne(
            {
                "aws_sub": aws_sub,
            },
            {
                "$set": {
                    "notes.$[x].isTrashed": false
                }
            },
            {
                "arrayFilters": [
                    {
                        "x._id": new ObjectId(uid)
                    }
                ]
            }
        );

        return response.json(status);
    },
};