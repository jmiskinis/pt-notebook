import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const updateScratchpadRoute = {
    path: "/api/v1/notes/scratchpad/:aws_sub",
    method: "patch",
    handler: async (request, response) => {
        const { aws_sub } = request.params;

        if (!aws_sub) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const { content } = request.body;
        const db = getDbConnection("pt-data");

        const status = await db.collection("notes").updateOne(
            {
                "aws_sub": aws_sub,
            },
            {
                "$set": {
                    "scratchpad.content": content,
                    "scratchpad.dateLastSaved": new Date().getTime(),
                }
            }
        );

        return response.json(status);
    },
};