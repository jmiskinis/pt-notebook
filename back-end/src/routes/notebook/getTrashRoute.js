import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const getTrashRoute = {
    path: "/api/v1/notes/trash/:aws_sub",
    method: "get",
    handler: async (request, response) => {
        const { aws_sub } = request.params;

        if (!aws_sub) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const notes = await db.collection("notes").aggregate([
            {
                "$match": {
                    "aws_sub": aws_sub
                }
            },
            {
                "$project": {
                    "notes": {
                        "$filter": {
                            "input": "$notes",
                            "as": "note",
                            "cond": {
                                "$eq": ["$$note.isTrashed", true]
                            }
                        }
                    }
                }
            }
        ]).toArray();

        if (notes) {
            return response.json(notes[0].notes);
        } else {
            return response.sendStatus(404);
        }
    },
};