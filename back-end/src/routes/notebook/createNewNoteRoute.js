import { ObjectId } from "mongodb";
import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const createNewNoteRoute = {
    path: "/api/v1/notes",
    method: "post",
    handler: async (request, response) => {
        const { aws_sub, heading, placeholder, content } = request.body;

        if (!aws_sub) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const status = await db.collection("notes").updateOne(
            {
                "aws_sub": aws_sub,
            },
            {
                "$push":
                {
                    "notes":
                    {
                        "$each": [{
                            "_id": new ObjectId(),
                            "heading": heading,
                            "placeholder": placeholder,
                            "content": content,
                            "dateLastSaved": new Date().getTime(),
                            "dateCreated": new Date().getTime(),
                            "isFavorite": false,
                            "isTrashed": false,
                        }],
                        "$sort": {
                            "dateLastSaved": -1
                        }
                    }
                }
            }
        );

        return response.json(status);
    },
};