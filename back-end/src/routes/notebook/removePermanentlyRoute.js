import { ObjectId } from "mongodb";
import { getDbConnection } from "../../db.js";
import verifyCognitoToken from "../../utils/verifyCognitoToken.js";

export const removePermanentlyRoute = {
    path: "/api/v1/notes/:uid/:aws_sub",
    method: "delete",
    handler: async (request, response) => {
        const { aws_sub, uid } = request.params;

        if (!aws_sub || !uid) {
            return response.status(401).json("Missing parameters");
        }

        const { authorization } = request.headers;

        try {
            await verifyCognitoToken(authorization);
        } catch {
            return response.status(401).json("Could not validate token");
        }

        const db = getDbConnection("pt-data");

        const status = await db.collection("notes").updateOne(
            {
                "aws_sub": aws_sub,
            },
            {
                "$pull":
                {
                    "notes": {
                        "_id": new ObjectId(uid)
                    }
                }
            }
        );

        return response.json(status);
    },
};