import { CognitoUser } from "amazon-cognito-identity-js";
import { awsUserPool } from "../../utils/awsUserPool.js";

export const forgotPasswordRoute = {
    path: "/api/v1/notes/forgot-password",
    method: "post",
    handler: async (request, response) => {
        const { email } = request.body;

        if (!email) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        new CognitoUser({ "Username": email, "Pool": awsUserPool })
            .forgotPassword({
                onSuccess: async result => {
                    response.status(200).json(result);
                },
                onFailure: async error => {
                    response.status(500).json(error);
                }
            });
    }
};