import { CognitoUser } from "amazon-cognito-identity-js";
import { getDbConnection } from "../../db.js";
import { awsUserPool } from "../../utils/awsUserPool.js";

export const verifyEmailRoute = {
    path: "/api/v1/notes/verify-email",
    method: "patch",
    handler: async (request, response) => {
        const { email, code } = request.body;

        if (!email || !code) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        new CognitoUser({ "Username": email, "Pool": awsUserPool })
            .confirmRegistration(code, true, async (error, awsResult) => {
                if (error) {
                    return response.status(400).json(error);
                }

                try {
                    // Initialize the new user in the database
                    const db = getDbConnection("pt-data");

                    // TODO: check if the fields we're adding do not exist.
                    // They might if we removed user via Cognito dashboard
                    // without removing their data in our database.
                    const status = await db.collection("notes").updateOne(
                        {
                            "email": email,
                        },
                        {
                            "$set": {
                                "scratchpad.content": "# This is your scratchpad\n\nUse it to store your unorganized notes.",
                                "scratchpad.dateLastSaved": "",
                                "notes": [],
                            }
                        });

                    return response.json(status);
                } catch (e) {
                    return response.status(500).json({
                        "message": "Could not connect to the database",
                    });
                }
            });
    }
}