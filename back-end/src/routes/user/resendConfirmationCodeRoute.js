import { CognitoUser } from "amazon-cognito-identity-js";
import { awsUserPool } from "../../utils/awsUserPool.js";

export const resendConfirmationCodeRoute = {
    path: "/api/v1/notes/resend-confirmation-code",
    method: "post",
    handler: async (request, response) => {
        const { email } = request.body;

        if (!email) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        new CognitoUser({ "Username": email, "Pool": awsUserPool })
            .resendConfirmationCode(async (error, awsResult) => {
                if (error) {
                    return response.status(400).json(error);
                }

                return response.json(awsResult);
            });
    }
}