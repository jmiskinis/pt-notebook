import { getDbConnection } from "../../db.js";

export const getUserEmailVerificationStatusRoute = {
    path: "/api/v1/notes/user-email-verification-status/:email",
    method: "get",
    handler: async (request, response) => {
        const { email } = request.params;

        if (!email) {
            return response.status(400).json({
                "message": "Missing parameters"
            });
        }

        const db = getDbConnection("pt-data");

        const user = await db.collection("notes").findOne({
            "email": email
        });

        if (!user) {
            return response.status(400).json({
                "status": "not-found"
            });
        } else if (user.scratchpad) {
            return response.json({
                "status": "verified"
            });
        } else {
            return response.json({
                "status": "unverified"
            });
        }
    },
};
