import { AuthenticationDetails, CognitoUser } from "amazon-cognito-identity-js";
import { awsUserPool } from "../../utils/awsUserPool.js";

export const signInRoute = {
    path: "/api/v1/notes/sign-in",
    method: "post",
    handler: async (request, response) => {
        const { email, password } = request.body;

        if (!email || !password) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        new CognitoUser({ Username: email, Pool: awsUserPool })
            .authenticateUser(new AuthenticationDetails({
                "Username": email,
                "Password": password
            }), {
                onSuccess: async result => {
                    return response.status(200).json({
                        "accessToken": result.getAccessToken().getJwtToken(),
                        "idToken": result.getIdToken().getJwtToken()
                    });
                },
                onFailure: async error => {
                    return response.json(error);
                }
            });
    }
};