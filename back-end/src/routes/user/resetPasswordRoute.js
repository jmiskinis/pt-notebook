import { CognitoUser } from "amazon-cognito-identity-js";
import { awsUserPool } from "../../utils/awsUserPool.js";

export const resetPasswordRoute = {
    path: "/api/v1/notes/reset-password/:code",
    method: "post",
    handler: async (request, response) => {
        const { code } = request.params;
        const { email, password } = request.body;

        if (!email || !password || !code) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        new CognitoUser({ "Username": email, "Pool": awsUserPool })
            .confirmPassword(code, password, {
                onSuccess: async result => {
                    return response.status(200).json(result);
                },
                onFailure: async error => {
                    return response.json(error);

                }
            });
    }
};