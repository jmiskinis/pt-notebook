import { CognitoUserAttribute } from "amazon-cognito-identity-js";
import { awsUserPool } from "../../utils/awsUserPool.js";
import { getDbConnection } from "../../db.js";

// TODO: what if user is verified in our db but removed in Cognito?
// We should check before replacing existing content.
export const signUpRoute = {
    path: "/api/v1/notes/sign-up",
    method: "post",
    handler: async (request, response) => {
        // Data received from the Sign Up page:
        const { email, password, name } = request.body;

        if (!email || !password) {
            return response.status(400).json({
                "message": "Missing parameters",
            });
        }

        // Attributes to be sent to AWS:
        const attributes = [
            new CognitoUserAttribute({ "Name": "email", "Value": email }),
            new CognitoUserAttribute({ "Name": "name", "Value": name ? name : "" }),
            new CognitoUserAttribute({ "Name": "custom:Theme", "Value": "auto" }),
        ];

        // Sign up using AWS Cognito:
        awsUserPool.signUp(email, password, attributes, null, async (error,
            awsResult) => {
            if (error) {
                return response.json(error);
            }

            try {
                const db = getDbConnection("pt-data");

                // Add user email and AWS identifier to our database:
                const status = await db.collection("notes").insertOne({
                    "email": awsResult.user.getUsername(),
                    "aws_sub": awsResult.userSub,
                });

                return response.json(status);
            } catch (error) {
                return response.status(500).json({
                    "message": "Could not connect to the database",
                    "error": error
                });
            }
        });
    }
}