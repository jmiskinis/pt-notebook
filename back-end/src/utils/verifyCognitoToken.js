import { CognitoJwtVerifier } from "aws-jwt-verify";

export default async function verifyCognitoToken(authorization) {
    if (!authorization) {
        return response.status(401).json({
            message: "No authorization sent"
        });
    }

    const verifier = CognitoJwtVerifier.create({
        userPoolId: process.env.AWS_USER_POOL_ID,
        tokenUse: "access",
        clientId: process.env.AWS_CLIENT_ID,
    });

    try {
        await verifier.verify(authorization.split(' ')[1]);
    } catch (error) {
        throw new Error(error);
    }

}
