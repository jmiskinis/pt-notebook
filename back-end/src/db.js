import { MongoClient } from "mongodb";

let client;

export const initializeDbConnection = async () => {
    client = await MongoClient.connect
        (
            `mongodb://${process.env.AWS_DOCUMENTDB_USER}:${process.env.AWS_DOCUMENTDB_PASSWORD}@pt-notebook.cluster-czea9brwyrgp.eu-central-1.docdb.amazonaws.com:27017/?tls=true&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false`,
            {
                tlsCAFile: `./rds-combined-ca-bundle.pem`
            },
        )
};

export const getDbConnection = dbName => {
    const db = client.db(dbName);
    return db;
};