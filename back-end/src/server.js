import express from "express";
import path from "path";
import { routes } from './routes/index.js';
import { initializeDbConnection } from './db.js';
import { fileURLToPath } from "url";

const PORT = process.env.PORT || 8080;
const app = express();

// Required path definitions because we set "type": "module"
// in our package.json
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

// Allows access to the body of POST/PUT requests in our route handlers
// (as request.body):
app.use(express.json());

// For our static website files:
app.use(express.static(path.join(__dirname, "../build")));

// For route requests that are not our API requests:
app.get("/^(?!\/api).+/", (request, response) => {
    response.sendFile(path.join(__dirname, "../build/index.html"));
});

// Add all the routes to Express server exported from routes/index.js:
routes.forEach(route => {
    app[route.method](route.path, route.handler);
});

// Connect to the database, then start the server:
initializeDbConnection().then(() => {
    app.listen(PORT, () => {
        console.log(`Server is listening on port ${PORT}`);
    });
});