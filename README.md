# PT Notebook
## Contents
This repository stores source files required to deploy a web note taking app. The project relies on MERN (MongoDB, Express, React, Node.js) stack and utilizes Javascript for both the back-end and front-end as well as a document-oriented or non relational database (MongoDB). The `front-end` folder provides files for React app and `back-end` contains files required to run an Express back-end. Instructions below describes how to build and deploy the project to AWS infrastructure, specifically using the following services:
```
AWS Elastic Beanstalk
Amazon DocumentDB
Amazon Cognito
```
## Preparing the Infrastructure
As described, the project relies on Amazon Web Services, therefore you should create AWS account if you haven't got one already.
### Amazon Cognito
Cognito is an identity store used in this project. It stores user information and provides them with tokens to access restricted areas of your web app.
1. Sign into AWS console and search for `Cognito` service in the search field at the top.
2. Create a new user pool by selecting `Add user directories to your app` and use Cognito defaults with these considerations in mind:
    * Select email only for sign in and user account recovery
    * Use no MFA
    * Add a custom and mutable custom string attribute `Theme`
    * Choose to send email with Cognito
    * Don't generate a client secret
3. Navigate to your newly created user pool.
4. Save your user pool id as `AWS_USER_POOL_ID=<your user pool id>` in a temporary text file only you can access. We will need this information during later stages of infrastructure preparation.
5. Select `App integration` and click on your app client name at the bottom.
6. Save your app client id in the same text file as `AWS_CLIENT_ID=<your app client id>`.
5. Expand `Attribute read and write permissions` and make sure that `custom:Theme` attribute is readable and writeable.
6. Navigate back to the main `Cognito` page.
7. Select `Grant access to AWS services` from the drop down and click `Create identity pool`.
8. Make sure to expand the `Authentication providers` tab below to add `User Pool ID` and `App client id` values to Cognito provider from the text file you have previously created.
9. Once you create the identity pool, make sure to copy the identity pool id from `Get AWS Credentials` (marked in red) and save it in a text file as `AWS_IDENTITY_POOL_ID=<your identity pool id>`.
10. Finally, add to the text file your AWS region, e.g. `eu-north-1`, as `AWS_REGION=eu-north-1`. The text file should ultimately contain these values:
```
AWS_USER_POOL_ID=<your user pool id>
AWS_CLIENT_ID=<your app client id>
AWS_IDENTITY_POOL_ID=<your identity pool id>
AWS_REGION=<your region>
```
### AWS Elastic Beanstalk
Amazon Elastic Beanstalk is an easy-to-use service for deploying and scaling web applications and services developed with Java, .NET, PHP, Node.js, Python, Ruby, Go, and Docker on familiar servers such as Apache, Nginx, Passenger, and IIS.
1. Sign into AWS console and search for `Elastic Beanstalk` service in the search field at the top.
2. Click on `Create application`.
3. Use defaults with these considerations in mind: 
    * Select Node.js as a platform
    * No integrated RDS SQL database (we use our own)
    * Auto scaling group: single instance or load balanced, depending on your needs
    * Add you Cognito environmental values from the text file you previously created
4. You will also need to set up a security group that will enable you to connect to your Amazon DocumentDB cluster on port 27017 (the default port for Amazon DocumentDB) from your AWS Elastic Beanstalk instance. Refer to [this AWS Developer Guide](https://docs.aws.amazon.com/documentdb/latest/developerguide/connect-ec2.html) for step-by-step instructions.
### Amazon DocumentDB
Amazon DocumentDB is a fast, scalable, highly available, and fully managed document database service that supports MongoDB workloads.
1. Sign into AWS console and search for `DocumentDB` service in the search field at the top.
2. Click on `Launch Amazon DocumentDB` and create a new Amazon DocumentDB cluster using options that fulfill your requirements. Make sure to select a correct security group in advanced settings (as described in [the Developer guide](https://docs.aws.amazon.com/documentdb/latest/developerguide/connect-ec2.html)).
3. Add your DocumentDB user name and password as `AWS_DOCUMENTDB_USER` and `AWS_DOCUMENTDB_PASSWORD` respectively to you AWS Elastic Beanstalk instance environmental values.
## Building
Here we assume that the user uses a Linux terminal. If you don't use Linux, look up the equivalent terminal commands used by other operating systems. Begin with opening the terminal. Clone the repository:
```
git clone https://gitlab.com/jmiskinis/pt-notebook.git
```
Then run the following commands:
```
cd pt-notebook/front-end
npm install
npm run build
```
The above will produce a `build` folder which contains static files for the React app. Move it to the back-end and install the dependencies:
```
mv build ../back-end/
cd ../back-end
npm install
```
Download the Amazon DocumentDB Certificate Authority (CA) certificate required to authenticate to your DocumentDB cluster:
```
wget https://s3.amazonaws.com/rds-downloads/rds-combined-ca-bundle.pem
```
In a web browser, navigate to your Amazon DocumentDB dashboard. Select your cluster, then click on `Connectivity & security` tab and copy the string under `Connect to this cluster with an application`. Open `pt-notebook/back-end/src/db.js` with a text editor of your choice and replace the MongoDB connection string with the one you have just copied. Make sure to only replace the part after the `@` symbol and keep the `${process.env.[...]}` values in place.

Use the `zip` tool to package everything in a ZIP file:
```
zip -r source.zip . -x .gitignore
```
The resulting `source.zip` file contains all our project files that can now be uploaded and deployed to AWS Elastic Beanstalk.
## Deploying
1. In a web browser, navigate to your AWS Elastic Beanstalk dashboard and select the project environment your have previously created.
2. Click the `Upload and deploy` button at the top right corner.
3. Click on `Choose file` and select the previously generated `source.zip` file.
4. Provide a version label and hit `Deploy`.
5. Upon successful environment update, you should be able to access the project using the domain name listed in the `Environment overview` widget.
