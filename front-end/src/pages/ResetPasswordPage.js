import axios from "axios";
import { useState, useEffect } from "react";
import { useNavigate } from "react-router-dom";
import { useQueryParams } from "../utils/useQueryParams.js";

export default function ResetPasswordPage() {
    const navigate = useNavigate();
    const { email } = useQueryParams();
    const [error, setError] = useState("");
    const [code, setCode] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [isValidPassword, setIsValidPassword] = useState(true);

    useEffect(() => {
        if (!email) {
            navigate("/sign-in");
        }
    }, [navigate, email]);

    const onSubmitForm = async (event) => {
        event.preventDefault();

        if (password !== repeatPassword) {
            setIsValidPassword(false);
            return;
        }

        try {
            const response = await axios.post(`/api/v1/notes/reset-password/${code}`, {
                "email": email,
                "password": password,
            });

            if (!response.data?.code) {
                navigate(`/sign-in?reset=true`);
            }

            switch (response.data.code) {
                case "InvalidPasswordException":
                    setIsValidPassword(false);
                    break;
                default:
                    setError("There was a problem. Please contact the administrator.");
            }
        } catch (error) {
            setError("There was a problem. Please contact the administrator.");
            console.error(error);
        }
    }

    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="sign-up-page">
                <form onSubmit={onSubmitForm} autoComplete="off">
                    <svg className="bi me-2" width="70" height="70"><use xlinkHref="#logo-icon" /></svg>
                    <h1 className="h3 mb-3 mt-3">Reset Your Password</h1>
                    <p>Enter a six-digit code that we have sent to your email and a new password.</p>
                    {!isValidPassword ?
                        <div className="alert alert-warning" role="alert" style={{ "textAlign": "left" }}>
                            <strong>Your password should:</strong><br />
                            - Be at least 8 characters in length<br />
                            - Contain at least 1 number<br />
                            - Contain at least 1 special character<br />
                            - Contain at least 1 uppercase letter<br />
                            - Contain at least 1 lowercase letter
                        </div>
                        :
                        ""
                    }
                    {error ?
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        :
                        ""
                    }
                    <div className="form-floating">
                        <input type="number" maxLength="6" id="code" className="top form-control" placeholder="Verification code" value={code} onChange={e => setCode(e.target.value)} required />
                        <label htmlFor="code">Verification code</label>
                    </div>
                    <div className="form-floating">
                        <input type="password" id="password" className="middle form-control" placeholder="New password" value={password} onChange={e => setPassword(e.target.value)} required autoComplete="off" />
                        <label htmlFor="password">New password</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" id="repeat-password" className="bottom form-control" placeholder="Repeat new password" value={repeatPassword} onChange={e => setRepeatPassword(e.target.value)} required autoComplete="off" />
                        <label htmlFor="repeat-password">Repeat new password</label>
                    </div>
                    <button className="w-100 btn btn-lg btn-primary mb-3" type="submit">Reset password</button>
                </form>
            </div >
        </div >
    )
}