import axios from "axios";
import { useState, useEffect, useCallback } from "react";
import { useNavigate } from "react-router-dom";
import Sidebar from "../components/Sidebar.js";
import NoteColumn from "../components/NoteColumn.js";
import Editor from "../components/Editor.js";
import * as noteFunctions from "../utils/noteFunctions.js";
import { useToken } from "../authentication/useToken.js";
import { useUser } from "../authentication/useUser.js";

export default function CreateNotePage() {
    const navigate = useNavigate();
    const [token] = useToken();
    const user = useUser().username;
    const [notes, setNotes] = useState(noteFunctions.initialNoteList);
    const [content, setContent] = useState("");
    const [active, setActive] = useState(null);

    const loadNotes = useCallback(async (activeId) => {
        try {
            const response = await axios.get(`/api/v1/notes/${user}`,
                {
                    "headers": { "Authorization": `Bearer ${token}` }
                }
            );

            const { data } = response;

            // Placeholder for the new note:
            data.unshift({
                "_id": "new-uid",
                "heading": "New Note",
                "placeholder": "Just start typing",
                "content": "",
                "dateLastSaved": null,
            });

            // Keep track of activated note between page loads:
            if (activeId) {
                const filtered = data.filter(note => note._id === activeId);

                if (filtered.length) {
                    setActive(filtered[0]);
                } else if (data.length) {
                    setActive(data[0]);
                } else {
                    setActive(null);
                }
            } else if (active === null) {
                setActive(data[0]);
            }

            setNotes(data);
        } catch (error) {
            console.error(error);
        }
    }, []);

    const createNewNote = useCallback(async () => {
        try {
            const status = await axios.post(`/api/v1/notes`,
                {
                    "aws_sub": user,
                    "heading": notes[0].heading,
                    "placeholder": notes[0].placeholder,
                    "content": notes[0].content,
                },
                {
                    "headers": { "Authorization": `Bearer ${token}` }
                });

            // Redirect to notes page after new note is created:
            navigate("/");

            if (status.data?.modifiedCount !== 1) {
                throw new Error("Could not add new note");
            }
        } catch (error) {
            console.error(error);
        }
    }, [notes]);

    // Load notes and focus the editor on initial page load:
    useEffect(() => {
        document.querySelector(".w-md-editor-text-input").focus();
        loadNotes();
    }, [loadNotes]);

    // Re-set editor content whenever active note updates:
    useEffect(() => {
        setContent(active ? active.content : "");
    }, [notes, active]);

    // Create a new note as a debounced event:
    useEffect(
        () => {
            const timeout = setTimeout(() => {
                if (notes[0].content) {
                    createNewNote();
                }
            }, noteFunctions.USER_TYPE_DEBOUNCE_DURATION);

            return () => clearTimeout(timeout);
        },
        [content]
    );

    return (
        <div className="row">
            <Sidebar />
            <NoteColumn
                notes={notes}
                active={active}
                setActive={setActive}
                loadNotes={loadNotes}
            />
            <Editor value={content} onChange={text =>
                noteFunctions.onEditorContentChange(
                    {
                        "text": text,
                        "active": active,
                        "setContent": setContent,
                    }
                )}
            />
        </div>
    )
}