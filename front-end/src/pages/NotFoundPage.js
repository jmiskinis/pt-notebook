import { Link } from "react-router-dom";

export default function NotFoundPage() {
    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="not-found-page">
                <h1 className="display-1 fw-bold">404</h1>
                <p className="fs-3"> <span className="text-primary">Opps!</span> Page not found.</p>
                <p className="lead">The page you are looking for does not exist.</p>
                <Link to="/" className="btn btn-primary">Take me to the homepage</Link>
            </div>
        </div>
    );
}