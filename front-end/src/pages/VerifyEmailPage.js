import axios from "axios";
import { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useQueryParams } from "../utils/useQueryParams.js";

export default function VerifyEmailPage() {
    const navigate = useNavigate();
    const [error, setError] = useState("");
    const [info, setInfo] = useState("");
    const [code, setCode] = useState("");
    const { email, redirect } = useQueryParams();

    useEffect(() => {
        if (!email) {
            navigate("/sign-in");
        }
    }, [navigate, email]);

    const onClickResendConfirmationCode = async (event) => {
        try {
            const response = await axios.post("/api/v1/notes/resend-confirmation-code", {
                "email": email
            });

            if (response.data?.CodeDeliveryDetails?.AttributeName === "email") {
                setInfo(`New confirmation code has been sent to ${email}. Just in case, please check your spam folder.`)
                event.target.remove();
            } else {
                setError("Could not resend a new confirmation code. Please contact the administrator.")
            }
        } catch (error) {
            console.error(error);
            setError("Could not resend a new confirmation code. Please contact the administrator.")
        }
    }

    const onSubmitForm = async (event) => {
        event.preventDefault();

        try {
            const response = await axios.patch("/api/v1/notes/verify-email", {
                "email": email,
                "code": code,
            });

            if (!response.data?.code) {
                navigate("/sign-in?verified=true");
            }

            switch (response.data.code) {
                case "CodeMismatchException":
                    setError("The confirmation code you entered is not correct. Please try again.");
                    break;
                default:
                    setError("There was a problem. Please contact the administrator.");
            }
        } catch (error) {
            console.error(error);
            setError("There was a problem. Please contact the administrator.");
        }
    }

    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="forgot-password-page">
                <form onSubmit={onSubmitForm}>
                    <svg className="bi me-2" width="70" height="70"><use xlinkHref="#logo-icon" /></svg>
                    <h1 className="h3 mb-3 mt-3">{redirect ? "Thank You!" : "Verify Your Email"}</h1>
                    <p>We have sent you an email with a six-digit confirmation code. Please enter it below to verify your email.</p>
                    {error
                        ?
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        :
                        ""
                    }
                    {info
                        ?
                        <div className="alert alert-primary" role="alert">
                            {info}
                        </div>
                        :
                        ""
                    }
                    <div className="form-floating mb-3">
                        <input type="number" maxLength="6" className="form-control" id="confirmation-code" placeholder="000000" value={code} onChange={e => setCode(e.target.value)} required />
                        <label htmlFor="confirmation-code">Confirmation code</label>
                    </div>
                    <button className="w-100 btn btn-lg btn-primary mb-3" type="submit">Verify</button>
                    <button className="w-100 btn btn-lg btn-primary mb-3" type="button" onClick={onClickResendConfirmationCode}>Resend confirmation code</button>
                </form>
            </div>
        </div >
    )
}