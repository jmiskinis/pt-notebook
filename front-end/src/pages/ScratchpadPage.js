import axios from "axios";
import { useState, useEffect, useCallback } from "react";
import Sidebar from "../components/Sidebar.js";
import Editor from "../components/Editor.js";
import * as noteFunctions from "../utils/noteFunctions.js";
import { useToken } from "../authentication/useToken.js";
import { useUser } from "../authentication/useUser.js";

export default function ScratchpadPage() {
    const [token] = useToken();
    const user = useUser().username;
    const [content, setContent] = useState("");

    useEffect(() => {
        const loadScratchpad = async () => {
            try {
                const response = await axios.get(`/api/v1/notes/scratchpad/${user}`,
                    {
                        "headers": { "Authorization": `Bearer ${token}` }
                    }
                );
                setContent(response.data.content);
            } catch (error) {
                console.log(error);
            }
        };

        loadScratchpad();
    }, []);

    const updateScratchpad = useCallback(async () => {
        try {
            const status = await axios.patch(`/api/v1/notes/scratchpad/${user}`,
                {
                    "content": content,
                },
                {
                    "headers": { "Authorization": `Bearer ${token}` }
                },
            );

            if (status.data?.modifiedCount !== 1) {
                throw new Error("Could not update scratchpad");
            }
        } catch (error) {
            console.error(error);
        }
    }, [content]);

    // Update scratchpad as a debounced event:
    useEffect(
        () => {
            const timeout = setTimeout(() => {
                if (content) {
                    updateScratchpad();
                }
            }, noteFunctions.USER_TYPE_DEBOUNCE_DURATION);

            return () => clearTimeout(timeout);
        },
        [content]
    );

    return (
        <div className="row">
            <Sidebar />
            <Editor value={content} onChange={setContent} preview="live" />
        </div>
    )
}