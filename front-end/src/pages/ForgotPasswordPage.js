import axios from "axios";
import { useState } from "react";
import { useNavigate } from "react-router-dom";

export default function ForgotPasswordPage() {
    const navigate = useNavigate();
    const [email, setEmail] = useState("");
    const [error, setError] = useState("");

    const onSubmitForm = async (event) => {
        event.preventDefault();

        try {
            const response = await axios.post(`/api/v1/notes/forgot-password`, {
                "email": email,
            });

            if (response.data?.CodeDeliveryDetails?.AttributeName === "email") {
                navigate(`/reset-password?email=${encodeURIComponent(email)}`);
            } else {
                setError("Could not send a new confirmation code. Please contact the administrator.")
            }
        } catch (error) {
            document.getElementById('submit-button').remove();
            setError(`There was a problem trying to reset password for ${email}. Please contact the administrator.`);
            console.error(error);
        }
    }

    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="forgot-password-page">
                <form onSubmit={onSubmitForm}>
                    <svg className="bi me-2" width="70" height="70"><use xlinkHref="#logo-icon" /></svg>
                    <h1 className="h3 mb-3 mt-3">Forgot Your Password?</h1>
                    <p>Enter your email address and we will send you a six-digit code to reset your password.</p>
                    {error ?
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        :
                        ""
                    }
                    <div className="form-floating mb-3">
                        <input type="email" id="email" className="form-control" placeholder="name@example.com" autoComplete="username" value={email} onChange={e => setEmail(e.target.value)} required />
                        <label htmlFor="email">Email address</label>
                    </div>
                    <button className="w-100 btn btn-lg btn-primary mb-3" id="submit-button" type="submit">Reset your password</button>
                </form>
            </div>
        </div >
    )
}