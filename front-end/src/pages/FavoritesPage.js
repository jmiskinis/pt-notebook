import axios from "axios";
import { useState, useEffect, useCallback } from "react";
import Sidebar from "../components/Sidebar.js";
import NoteColumn from "../components/NoteColumn.js";
import Editor from "../components/Editor.js";
import * as noteFunctions from "../utils/noteFunctions.js";
import { useToken } from "../authentication/useToken.js";
import { useUser } from "../authentication/useUser.js";

export default function NotesPage() {
    const [token] = useToken();
    const user = useUser().username;
    const [notes, setNotes] = useState(noteFunctions.initialNoteList);
    const [content, setContent] = useState("");
    const [active, setActive] = useState(null);

    const loadNotes = useCallback(async (activeId) => {
        try {
            const response = await axios.get(`/api/v1/notes/favorites/${user}`,
                {
                    "headers": { "Authorization": `Bearer ${token}` }
                }
            );

            const { data } = response;

            // Keep track of activated note between page loads:
            if (activeId) {
                const filtered = data.filter(note => note._id === activeId);

                if (filtered.length) {
                    setActive(filtered[0]);
                } else if (data.length) {
                    setActive(data[0]);
                } else {
                    setActive(null);
                }
            } else if (active === null) {
                setActive(data[0]);
            }

            setNotes(response.data);
        } catch (error) {
            console.error(error);
        }
    }, []);

    const updateNote = useCallback(async (active) => {
        try {
            const uid = active._id;

            const status = await axios.patch(`/api/v1/notes/${uid}`,
                {
                    "aws_sub": user,
                    "heading": active.heading,
                    "placeholder": active.placeholder,
                    "content": active.content,
                },
                {
                    "headers": { "Authorization": `Bearer ${token}` }
                }
            );

            if (status.data?.modifiedCount !== 1) {
                throw new Error("Could not update note");
            }

            loadNotes(active._id);
        } catch (error) {
            console.error(error);
        }
    }, []);

    // Load notes on initial page load:
    useEffect(() => {
        loadNotes();
    }, []);

    // Re-set editor content whenever active note updates:
    useEffect(() => {
        setContent(active ? active.content : "");
    }, [active]);

    // Update a note as a debounced event:
    useEffect(
        () => {
            const timeout = setTimeout(() => {
                // Only update when editor is focused:
                if (active?.content && document.activeElement ===
                    document.querySelector(".w-md-editor-text-input")) {
                    updateNote(active);
                }
            }, noteFunctions.USER_TYPE_DEBOUNCE_DURATION);

            return () => clearTimeout(timeout);
        },
        [content]
    );

    return (
        <div className="row">
            <Sidebar />
            <NoteColumn
                notes={notes}
                active={active}
                setActive={setActive}
                loadNotes={loadNotes}
            />
            <Editor value={content} onChange={text =>
                noteFunctions.onEditorContentChange(
                    {
                        "text": text,
                        "active": active,
                        "setContent": setContent,
                    }
                )}
            />
        </div>
    )
}