import axios from "axios";
import { useState } from "react";
import { useNavigate, Link, useLocation } from "react-router-dom";
import { useQueryParams } from "../utils/useQueryParams.js";
import { useToken } from "../authentication/useToken.js"
import { useUserAttributes } from "../authentication/useUserAttributes.js"

export default function SignInPage() {
    const navigate = useNavigate();
    const location = useLocation();
    const [, setToken] = useToken();
    const [, setUserAttributes] = useUserAttributes();
    const [error, setError] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const { verified } = useQueryParams();
    const { reset } = useQueryParams();

    const onSubmitForm = async (event) => {
        event.preventDefault();

        try {
            const response = await axios.post("/api/v1/notes/sign-in", {
                "email": email,
                "password": password,
            });

            const { data } = response;

            if (data.code) {
                switch (data.code) {
                    case "NotAuthorizedException":
                        setError("Wrong credentials. Please try again.")
                        break;
                    default:
                        setError("There was a problem with your authentication. Please contact the administrator.")
                }
            } else if (data.accessToken && data.idToken) {
                setToken(data.accessToken);
                setUserAttributes(data.idToken);
                navigate(location.state?.from ? location.state.from : "/");
            } else {
                setError("Could not access authentication endpoint. Please contact the administrator.")
            }
        } catch (error) {
            console.error(error);
            setError("There was a problem. Please contact the administrator.");
        }

    }

    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="log-in-page">
                <form onSubmit={onSubmitForm}>
                    <svg className="bi me-2" width="70" height="70"><use xlinkHref="#logo-icon" /></svg>
                    <h1 className="h3 mb-3 mt-3">Sign In to Notebook</h1>
                    {verified
                        ?
                        <div className="alert alert-success" role="alert">
                            Your email has been verified! You can now sign in with your credentials.
                        </div>
                        :
                        ""
                    }
                    {reset
                        ?
                        <div className="alert alert-success" role="alert">
                            Your password has been reset! You can now sign in with your new credentials.
                        </div>
                        :
                        ""
                    }
                    {error ?
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        :
                        ""
                    }
                    <div className="form-floating">
                        <input type="email" id="email" className="top form-control" placeholder="name@example.com" autoComplete="username" value={email} onChange={e => setEmail(e.target.value)} required />
                        <label htmlFor="email">Email address</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" id="password" className="bottom form-control" placeholder="Password" autoComplete="current-password" value={password} onChange={e => setPassword(e.target.value)} required />
                        <label htmlFor="password">Password</label>
                    </div>
                    {/* <div className="d-flex mb-3 justify-content-between">
                        <div className="checkbox">
                            <label>
                                <input type="checkbox" value="remember-me" /> Remember me
                            </label>
                        </div>
                        <Link className="text-decoration-none" to="/forgot-password">Forgot your password?</Link>
                    </div> */}
                    <button className="w-100 btn btn-lg btn-primary mb-3" type="submit">Sign in</button>
                    {verified
                        ?
                        ""
                        :
                        <p><Link className="text-decoration-none" to="/sign-up">Don't have an account? Sign up</Link></p>
                    }
                    <p><Link className="text-decoration-none" to="/forgot-password">Forgot your password?</Link></p>
                </form>
            </div>
        </div>
    )
}