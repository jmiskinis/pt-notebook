import axios from "axios";
import { useState } from "react";
import { useNavigate, Link } from "react-router-dom";

export default function SignUpPage() {
    const navigate = useNavigate();
    const [error, setError] = useState("");
    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [repeatPassword, setRepeatPassword] = useState("");
    const [isValidPassword, setIsValidPassword] = useState(true);

    const onSubmitForm = async (event) => {
        event.preventDefault();

        if (password !== repeatPassword) {
            setIsValidPassword(false);
            return;
        }

        try {
            const response = await axios.post("/api/v1/notes/sign-up", {
                "name": name,
                "email": email,
                "password": password,
            });

            if (!response.data?.code) {
                navigate(`/verify-email?email=${encodeURIComponent(email)}&redirect=true`);
            }

            switch (response.data.code) {
                case "UsernameExistsException":
                    const response = await axios.get(`/api/v1/notes/user-email-verification-status/${email}`);

                    if (response.status === "unverified") {
                        navigate(`/verify-email?email=${encodeURIComponent(email)}`);
                    }

                    setError("A user with this email already exists.");
                    break;
                case "InvalidPasswordException":
                    setIsValidPassword(false);
                    break;
                default:
                    setError("There was a problem. Please contact the administrator.");
            }
        } catch (error) {
            console.log(error);
        }
    }

    return (
        <div className="row justify-content-center dvh-100 align-items-center">
            <div className="form-auth text-center" id="sign-up-page">
                <form onSubmit={onSubmitForm} autoComplete="off">
                    <svg className="bi me-2" width="70" height="70"><use xlinkHref="#logo-icon" /></svg>
                    <h1 className="h3 mb-3 mt-3">Sign Up for Notebook</h1>
                    {!isValidPassword ?
                        <div className="alert alert-warning" role="alert" style={{ "textAlign": "left" }}>
                            <strong>Your password should:</strong><br />
                            - Be at least 8 characters in length<br />
                            - Contain at least 1 number<br />
                            - Contain at least 1 special character<br />
                            - Contain at least 1 uppercase letter<br />
                            - Contain at least 1 lowercase letter<br />
                            - Match the password you repeated
                        </div>
                        :
                        ""
                    }
                    {error ?
                        <div className="alert alert-danger" role="alert">
                            {error}
                        </div>
                        :
                        ""
                    }
                    <div className="form-floating">
                        <input type="text" id="name" className="top form-control" placeholder="Full Name" value={name} onChange={e => setName(e.target.value)} autoComplete="name" />
                        <label htmlFor="name">Full name</label>
                    </div>
                    <div className="form-floating">
                        <input type="email" id="email" className="middle form-control" placeholder="name@example.com" value={email} onChange={e => setEmail(e.target.value)} autoComplete="email" required />
                        <label htmlFor="email">Email address</label>
                    </div>
                    <div className="form-floating">
                        <input type="password" id="password" className="middle form-control" placeholder="Password" value={password} onChange={e => setPassword(e.target.value)} autoComplete="new-password" required />
                        <label htmlFor="password">Password</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input type="password" id="repeat-password" className="bottom form-control" placeholder="Repeat Password" value={repeatPassword} onChange={e => setRepeatPassword(e.target.value)} autoComplete="new-password" required />
                        <label htmlFor="repeat-password">Repeat password</label>
                    </div>
                    <div className="checkbox mb-3">
                        <label>
                            <input type="checkbox" value="user-terms" required /> I agree to the Terms of User
                        </label>
                    </div>
                    <button className="w-100 btn btn-lg btn-primary mb-3" type="submit">Sign up</button>
                    <Link className="text-decoration-none" to="/sign-in">Already have an account? Sign in</Link>
                </form>
            </div >
        </div >
    )
}