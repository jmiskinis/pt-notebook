import { Routes, Route } from 'react-router-dom';
import PrivateRoute from './authentication/PrivateRoute.js';
import PublicRoute from './authentication/PublicRoute.js';
import CreateNewNotePage from './pages/CreateNewNotePage.js';
import NotesPage from './pages/NotesPage.js';
import ScratchpadPage from './pages/ScratchpadPage.js';
import FavoritesPage from './pages/FavoritesPage.js';
import TrashPage from './pages/TrashPage.js';
import NotFoundPage from './pages/NotFoundPage.js';
import SignInPage from './pages/SignInPage.js';
import SignUpPage from './pages/SignUpPage.js';
import VerifyEmailPage from './pages/VerifyEmailPage.js';
import ForgotPasswordPage from './pages/ForgotPasswordPage.js';
import ResetPasswordPage from './pages/ResetPasswordPage.js';
import Icons from './components/Icons.js';
import ThemeProvider from './components/ThemeProvider.js';
import './style.css';

function App() {
  return (
    <ThemeProvider>
      <div className="app">
        <Icons />
        <div className="container-fluid">
          <Routes>
            <Route element={<PrivateRoute />}>
              <Route index element={<NotesPage />} />
              <Route path="/create" element={<CreateNewNotePage />} />
              <Route path="/scratchpad" element={<ScratchpadPage />} />
              <Route path="/favorites" element={<FavoritesPage />} />
              <Route path="/trash" element={<TrashPage />} />
            </Route>
            <Route element={<PublicRoute />}>
              <Route path="/sign-in" element={<SignInPage />} />
              <Route path="/sign-up" element={<SignUpPage />} />
              <Route path="/verify-email" element={<VerifyEmailPage />} />
              <Route path="/forgot-password" element={<ForgotPasswordPage />} />
              <Route path="/reset-password" element={<ResetPasswordPage />} />
            </Route>
            <Route path="*" element={<NotFoundPage />} />
          </Routes>
        </div>
      </div>
    </ThemeProvider>
  );
}

export default App;
