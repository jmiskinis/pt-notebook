import { default as MDtoText } from "markdown-to-text";

function stripTrailingDots(text) {
    while (text[text.length - 1] === ".") {
        text = text.slice(0, -1);
    }

    return text;
}

export function extractHeadingAndPlaceholder(text) {
    let firstParagraph;
    let splitLimit = 1;
    let counter = 0;
    let paragraphs = text.split('\n', splitLimit);

    while (paragraphs.length > counter) {
        const paragraphCount = paragraphs.length;
        const lastParagraph = paragraphs[paragraphCount - 1];

        if (lastParagraph !== "" && !firstParagraph) {
            firstParagraph = lastParagraph;
        }

        if (paragraphCount > 1 && lastParagraph !== ""
            && firstParagraph !== lastParagraph) {
            break;
        }

        paragraphs = text.split('\n', ++splitLimit);
        counter++;
    }

    const nonEmptyParagraphs = paragraphs.filter(line => line !== "");

    if (!nonEmptyParagraphs.length) {
        return { heading: "", placeholder: "" };
    }

    const sentences = nonEmptyParagraphs[0].split('. ');

    if (sentences.length > 1) {
        return {
            heading: stripTrailingDots(MDtoText(sentences[0])),
            placeholder: stripTrailingDots(MDtoText(nonEmptyParagraphs[0])),
        };
    }

    return {
        heading: stripTrailingDots(MDtoText(nonEmptyParagraphs[0])),
        placeholder: stripTrailingDots(MDtoText(nonEmptyParagraphs[1])),
    };
}