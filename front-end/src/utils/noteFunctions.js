import axios from "axios";
import { extractHeadingAndPlaceholder } from "./textExtractor.js"

// Time to wait in milliseconds after user stops
// typing to save note (changes) in the database:
export const USER_TYPE_DEBOUNCE_DURATION = 1000;

export const initialNoteList = [{
    "_id": "initial-uid",
    "heading": "",
    "placeholder": "",
    "content": "",
    "dateLastSaved": null,
    "isFavorite": false,
    "isTrashed": false,
}];

export function onEditorContentChange({ text, active, setContent }) {
    const { heading, placeholder } = extractHeadingAndPlaceholder(text);
    active["heading"] = heading;
    active["placeholder"] = placeholder;
    active["content"] = text;
    setContent(text);
}

export async function markAsFavorite(activeId, user, token, uid, callback) {
    try {
        const status = await axios.patch(`/api/v1/notes/${uid}/mark-as-favorite`,
            {
                "aws_sub": user,
            },
            {
                "headers": { "Authorization": `Bearer ${token}` }
            }
        );

        if (status.data?.modifiedCount !== 1) {
            console.log(status);
            throw new Error("Could not mark note as favorite");
        }

        callback(activeId);
    } catch (error) {
        console.error(error);
    }
}

export async function removeFromFavorites(activeId, user, token, uid, callback) {
    try {
        const status = await axios.patch(`/api/v1/notes/${uid}/remove-from-favorites`,
            {
                "aws_sub": user,
            },
            {
                "headers": { "Authorization": `Bearer ${token}` }
            }
        );

        if (status.data?.modifiedCount !== 1) {
            throw new Error("Could not remove note from favorites");
        }

        callback(activeId);
    } catch (error) {
        console.error(error);
    }
}

export async function markAsTrashed(activeId, user, token, uid, callback) {
    try {
        const status = await axios.patch(`/api/v1/notes/${uid}/mark-as-trashed`,
            {
                "aws_sub": user,
            },
            {
                "headers": { "Authorization": `Bearer ${token}` }
            }
        );

        if (status.data?.modifiedCount !== 1) {
            throw new Error("Could not mark note as trashed");
        }

        callback(activeId);
    } catch (error) {
        console.error(error);
    }
}

export async function restoreFromTrashed(activeId, user, token, uid, callback) {
    try {
        const status = await axios.patch(`/api/v1/notes/${uid}/restore-from-trashed`,
            {
                "aws_sub": user,
            },
            {
                "headers": { "Authorization": `Bearer ${token}` }
            }
        );

        if (status.data?.modifiedCount !== 1) {
            throw new Error("Could not restore note from trash");
        }

        callback(activeId);
    } catch (error) {
        console.error(error);
    }
};

export async function removePermanently(user, token, uid, callback) {
    try {
        const status = await axios.delete(`/api/v1/notes/${uid}/${user}`,
            {
                "headers": { "Authorization": `Bearer ${token}` }
            }
        );

        if (status.data?.modifiedCount !== 1) {
            throw new Error("Could not remove note from the database");
        }

        callback();
    } catch (error) {
        console.log(error);
    }
};