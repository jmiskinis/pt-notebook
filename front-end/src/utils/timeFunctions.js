const epochs = [
    ["year", 31536000],
    ["month", 2592000],
    ["day", 86400],
    ["hour", 3600],
    ["minute", 60],
    ["second", 1]
];

function getDuration(timeAgoInSeconds) {
    for (let [name, seconds] of epochs) {
        const interval = Math.floor(timeAgoInSeconds / seconds);

        if (interval >= 1) {
            return {
                interval: interval,
                epoch: name
            };
        }
    }
}

export default function timeSince(date) {
    const timeAgoInSeconds = Math.floor((new Date().getTime() - date) / 1000);

    if (timeAgoInSeconds === 0) {
        return "Just now";
    }

    const { interval, epoch } = getDuration(timeAgoInSeconds);
    const suffix = interval === 1 ? "" : "s";
    return `${interval} ${epoch}${suffix} ago`;
}