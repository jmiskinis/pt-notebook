import { Navigate, Outlet, useLocation } from "react-router-dom";
import { useUser } from "./useUser.js";

export default function PrivateRoute() {
    const user = useUser();
    const location = useLocation();
    return user ? <Outlet /> : <Navigate to="/sign-in" state={{ from: location }} />;
}