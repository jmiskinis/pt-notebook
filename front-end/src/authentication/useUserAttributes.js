import { useState } from 'react';

export const useUserAttributes = () => {
    const [attributes, setUserAttributesInternal] = useState(() => {
        const token = localStorage.getItem('idToken');

        if (!token) {
            return null;
        }

        const encodedPayload = token.split('.')[1];
        return JSON.parse(atob(encodedPayload));
    });

    const setUserAttributes = newToken => {
        localStorage.setItem('idToken', newToken);
        setUserAttributesInternal(newToken);
    }

    return [attributes, setUserAttributes];
}