import { Navigate, Outlet } from "react-router-dom";
import { useUser } from "./useUser.js";

export default function PublicRoute() {
    const user = useUser();
    return !user ? <Outlet /> : <Navigate to="/" replace />;
}