import { useState, useEffect, useContext } from "react";
import { ThemeContext } from "../components/ThemeProvider.js";

export default function UserProfile({ attributes }) {
    const [name, setName] = useState(attributes.name);
    const { themeSwitcher } = useContext(ThemeContext);

    const onSubmitForm = (event) => {
        event.preventDefault();
    }

    // Load notes on initial page load:
    useEffect(() => {
        document.getElementById("user-theme").value = attributes["custom:Theme"];
    }, []);

    return (
        <div className="modal fade" id="userProfileModal" tabIndex="-1" aria-labelledby="userProfileLabel" aria-hidden="true">
            <div className="modal-dialog">
                <div className="modal-content">
                    <div className="modal-header">
                        <h1 className="modal-title fs-5" id="userProfileLabel">User Profile</h1>
                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form onSubmit={onSubmitForm}>
                        <div className="modal-body">
                            <div className="form-floating mb-2">
                                <input type="name" className="form-control" id="user-name" placeholder="Your name" value={name} onChange={(e) => setName(e.target.value)} />
                                <label htmlFor="user-name">Your name</label>
                            </div>
                            <div className="form-floating">
                                <select onChange={themeSwitcher} className="form-select" id="user-theme" aria-label="Theme">
                                    <option value="auto">Auto</option>
                                    <option value="light">Light</option>
                                    <option value="dark">Dark</option>
                                </select>
                                <label htmlFor="user-theme">Select theme</label>
                            </div>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                            <button type="submit" className="btn btn-primary" disabled>Save changes</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    )
}