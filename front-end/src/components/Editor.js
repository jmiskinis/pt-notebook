import MDEditor from "@uiw/react-md-editor";

export default function Editor({ value, onChange, preview }) {
    return (
        <div className="col d-flex p-3 flex-column h-md-100" id="editor">
            <MDEditor height="100%" value={value} preview={preview ? preview : "edit"} onChange={onChange} />
        </div>
    )
}