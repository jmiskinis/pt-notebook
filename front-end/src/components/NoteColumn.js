import { useState } from 'react';
import NoteRow from './NoteRow.js';
import timeSince from '../utils/timeFunctions.js';

export default function NoteColumn(props) {
    const [searchValue, setSearchValue] = useState("");

    return (
        <div className="col-md-4 col-xl-3 h-md-100 p-3 d-flex flex-column shadow scrollarea" id="notes">
            <input type="text" className="form-control mb-3" id="search-notes-input" value={searchValue} onChange={e => setSearchValue(e.target.value)} placeholder="Search for notes" />
            <div className="list-group list-group-flush">
                {
                    props.notes.filter(note => note["content"].match(new RegExp(searchValue, "i"))).map((note, i) =>
                        <NoteRow
                            key={note._id}
                            uid={note._id}
                            index={i}
                            note={note}
                            heading={note.heading}
                            placeholder={note.placeholder}
                            content={note.content}
                            dateLastSaved={note.dateLastSaved ? timeSince(note.dateLastSaved) : "Not saved yet"}
                            isFavorite={note.isFavorite}
                            isTrashed={note.isTrashed}
                            active={props.active}
                            setActive={props.setActive}
                            loadNotes={props.loadNotes}
                        />
                    )
                }
            </div>
        </div>
    )
}