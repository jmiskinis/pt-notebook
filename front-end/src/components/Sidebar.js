import { useContext } from "react";
import { NavLink, useNavigate } from "react-router-dom";
import { ThemeContext } from "./ThemeProvider.js";
import UserProfile from "../authentication/UserProfile.js";
import { useUserAttributes } from "../authentication/useUserAttributes.js";

export default function Sidebar() {
    const navigate = useNavigate();
    const [attributes] = useUserAttributes();
    const { theme, invTheme } = useContext(ThemeContext);

    const signOut = () => {
        localStorage.removeItem("token");
        navigate("/sign-in");
    }

    return (
        <>
            <UserProfile attributes={attributes} />
            <div className={`col-md-3 col-xl-2 d-flex flex-md-column flex-row p-3 sticky-top bg-${theme} shadow`} id="sidebar">
                <NavLink to="/" className={`navbar-brand d-flex link-${invTheme} text-decoration-none`}>
                    <svg className="bi d-none d-md-block" width="40" height="32"><use xlinkHref="#logo-icon" /></svg>
                    <span className="fs-4 me-2">Notebook</span>
                </NavLink>
                <hr className="d-none d-md-block" />
                <ul className="nav nav-pills flex-md-column flex-row flex-md-grow-0 flex-grow-1 flex-nowrap justify-content-center">
                    <li className="nav-item mb-md-1 me-1 me-md-0">
                        <NavLink to="/create" className={`nav-link new-note py-md-3`}>
                            <svg className="bi me-md-2" width="16" height="16"><use xlinkHref="#add-a-note-icon" /></svg>
                            <span className="d-none d-md-inline">Create new note</span>
                        </NavLink>
                    </li>
                    <li className="nav-item mb-md-1 me-1 me-md-0">
                        <NavLink to="/scratchpad" className={`nav-link`}>
                            <svg className="bi me-md-2" width="16" height="16"><use xlinkHref="#scratchpad-icon" /></svg>
                            <span className="d-none d-md-inline">Scratchpad</span>
                        </NavLink>
                    </li>
                    <li className="nav-item mb-md-1 me-1 me-md-0">
                        <NavLink to="/" className={`nav-link`} end>
                            <svg className="bi me-md-2" width="16" height="16"><use xlinkHref="#notes-icon" /></svg>
                            <span className="d-none d-md-inline">Notes</span>
                        </NavLink>
                    </li>
                    <li className="nav-item mb-md-1 me-1 me-md-0">
                        <NavLink to="/favorites" className={`nav-link`}>
                            <svg className="bi me-md-2" width="16" height="16"><use xlinkHref="#favorites-icon" /></svg>
                            <span className="d-none d-md-inline">Favorites</span>
                        </NavLink>
                    </li>
                    <li className="nav-item mb-md-1 me-1 me-md-0">
                        <NavLink to="/trash" className={`nav-link`}>
                            <svg className="bi me-md-2" width="16" height="16"><use xlinkHref="#trash-icon" /></svg>
                            <span className="d-none d-md-inline">Trash</span>
                        </NavLink>
                    </li>
                </ul>
                <div className="dropdown mt-md-auto">
                    <hr className="d-none d-md-block" />
                    <span className={`d-flex align-items-center link-${invTheme} dropdown-toggle`}
                        id="user" data-bs-toggle="dropdown" aria-expanded="false">
                        <svg className="bi me-md-2" width="16" height="24"><use xlinkHref="#people-circle-icon" /></svg>
                        <span className="d-none d-md-inline">{attributes.name ? attributes.name : attributes.email}</span>
                    </span>
                    <ul className="dropdown-menu text-small shadow" aria-labelledby="user">
                        <li><span className="dropdown-item" data-bs-toggle="modal" data-bs-target="#userProfileModal">Profile</span></li>
                        <li>
                            <hr className="dropdown-divider" />
                        </li>
                        <li><span className="dropdown-item" onClick={signOut}>Sign out</span></li>
                    </ul>
                </div>
            </div>
        </>
    );
};