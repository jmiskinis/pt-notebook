import { useToken } from "../authentication/useToken.js";
import { useUser } from "../authentication/useUser.js";
import * as noteFunctions from "../utils/noteFunctions.js";

// TODO: add aria-current="true"

export default function NoteRow(
    {
        uid,
        note,
        index,
        active,
        setActive,
        heading,
        placeholder,
        content,
        dateLastSaved,
        isFavorite,
        isTrashed,
        loadNotes,
    }) {

    const [token] = useToken();
    const user = useUser().username;
    const fileName = `${heading}.md`;
    const fileHref = `data:text/html;charset=utf-8,${encodeURIComponent(content)}`;

    return (
        <div onClick={() => setActive(note)} className={`list-group-item list-group-item-action ${active && active?._id === uid ? 'active' : ''} py-3 lh-sm`}>
            <div className="d-flex w-100 align-items-center justify-content-between ">
                <div className="note-heading pe-2">
                    {isFavorite ? <svg className="bi me-2" width="16" height="16"><use xlinkHref="#favorites-icon" /></svg> : ""}
                    <strong className="mb-1">{heading}</strong>
                </div>
                <small className='date-last-saved'>{dateLastSaved}</small>
            </div>
            <div className="d-flex flex-row align-items-end justify-content-between">
                <div className="col-10 mb-1 small note-placeholder">{placeholder}</div>
                <div className="dropdown">
                    <svg className="bi dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false" width="24" height="24"><use xlinkHref="#properties-icon" /></svg>
                    <ul className="dropdown-menu text-small shadow" aria-labelledby="user">
                        {(() => {
                            if (!isTrashed) {
                                if (isFavorite) {
                                    return (
                                        <>
                                            <li>
                                                <div className="d-flex flex-row align-items-center justify-content-between dropdown-item">
                                                    <svg className="bi me-2" width="16" height="16"><use xlinkHref="#favorites-icon" /></svg>
                                                    <span onClick={() => noteFunctions.removeFromFavorites(active?._id, user, token, uid, loadNotes)}>Remove from favorites</span>
                                                </div>
                                            </li>
                                            <div className="dropdown-divider"></div>
                                        </>

                                    )
                                } else {
                                    return (
                                        <>
                                            <li>
                                                <div className={`d-flex flex-row align-items-center dropdown-item ${uid === 'new-uid' ? 'disabled' : ''}`}>
                                                    <svg className="bi me-2" width="16" height="16"><use xlinkHref="#favorites-icon" /></svg>
                                                    <span onClick={() => noteFunctions.markAsFavorite(active?._id, user, token, uid, loadNotes)}>Mark as favorite</span>
                                                </div>
                                            </li>
                                            <div className="dropdown-divider"></div>
                                        </>
                                    )
                                }
                            }
                        })()}
                        {isTrashed
                            ?
                            <>
                                <li>
                                    <div className="d-flex flex-row align-items-center dropdown-item">
                                        <svg className="bi me-2 text-danger" width="16" height="16"><use xlinkHref="#remove-icon" /></svg>
                                        <span onClick={() => noteFunctions.removePermanently(user, token, uid, loadNotes)}>Remove permanently</span>
                                    </div>
                                </li>
                                <div className="dropdown-divider"></div>
                                <li>
                                    <div className="d-flex flex-row align-items-center dropdown-item">
                                        <svg className="bi me-2" width="16" height="16"><use xlinkHref="#restore-icon" /></svg>
                                        <span onClick={() => noteFunctions.restoreFromTrashed(active?._id, user, token, uid, loadNotes)}>Restore from trash</span>
                                    </div>
                                </li>
                                <div className="dropdown-divider"></div>
                            </>
                            :
                            <>
                                <li>
                                    <div className={`d-flex flex-row align-items-center dropdown-item ${uid === 'new-uid' ? 'disabled' : ''}`}>
                                        <svg className="bi me-2" width="16" height="16"><use xlinkHref="#trash-icon" /></svg>
                                        <span onClick={() => noteFunctions.markAsTrashed(active?._id, user, token, uid, loadNotes)}>Move to trash</span>
                                    </div>
                                </li>
                                <div className="dropdown-divider"></div>
                            </>
                        }
                        <li>
                            <div className={`d-flex flex-row align-items-center dropdown-item ${uid === 'new-uid' ? 'disabled' : ''}`}>
                                <svg className="bi me-2" width="16" height="16"><use xlinkHref="#download-icon" /></svg>
                                <a className="text-decoration-none" download={fileName} href={fileHref}>Download</a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div >
        </div >
    );
}