import { createContext, useState, useEffect } from "react";

export const ThemeContext = createContext();

// TODO: we should rely on Cognito attribute, not localstorage
export default function ThemeProvider(props) {
    const [themeString, setThemeString] = useState("");
    const invThemeString = themeString === "dark" ? "light" : "dark";

    const storedTheme = localStorage.getItem("theme");

    const getPreferredTheme = () => {
        if (storedTheme) {
            return storedTheme;
        }

        return window.matchMedia("(prefers-color-scheme: dark)").matches ? "dark" : "light";
    }

    const setTheme = theme => {
        if (theme === "auto" && window.matchMedia("(prefers-color-scheme: dark)").matches) {
            document.documentElement.setAttribute("data-bs-theme", "dark");
            document.documentElement.setAttribute("data-color-mode", "dark")
        } else {
            document.documentElement.setAttribute("data-bs-theme", theme);
            document.documentElement.setAttribute("data-color-mode", theme)
        }
    }

    const themeSwitcher = event => {
        localStorage.setItem("theme", event.target.value);
        setTheme(event.target.value);
        setThemeString(event.target.value);
    };

    useEffect(() => {
        setTheme(getPreferredTheme());

        window.matchMedia("(prefers-color-scheme: dark)").addEventListener("change", () => {
            if (storedTheme !== "light" || storedTheme !== "dark") {
                setTheme(getPreferredTheme());
            }
        });
    }, []);

    return (
        <ThemeContext.Provider value={{ themeString, invThemeString, themeSwitcher }}>
            {props.children}
        </ThemeContext.Provider>
    );
};